/*********** Generated on 2010/11/27 12:40:19 by reswrap version 5.1.1 *********/

/* Created by reswrap from file E:\Mes Developpements\scetoexe\demos\example.sce */
const char FILE_EMBEDDED[176]=
  "//\r\n"
  "// Allan CORNET - 2010\r\n"
  "//\r\n"
  "\r\n"
  "disp('Hello from a .sce\");\r\n"
  "\r\n"
  "A = ones(5, 3);\r\n"
  "disp(\"A = \");\r\n"
  "B = rand(5, 3);\r\n"
  "disp(\"B = \");\r\n"
  "C = A * B;\r\n"
  "disp(\"C = A * B\");\r\n"
  "disp(\"C = \");\r\n"
  ""
  ;

const char FILENAME_EMBEDDED[] = "example.sce";