//==============================================================================
/* Allan CORNET - 2010 */
//==============================================================================
#include <stdlib.h>
#include <Windows.h>
#include "FileExists.h"
#include "getScilabPath.hxx"
#include "scilabWrap.h"
//==============================================================================
#define DLL_NAME_LIBSCILAB "libscilab.dll"
#define DLL_NAME_CALLSCILAB "call_scilab.dll"
#define DLL_NAME_API_SCILAB "api_scilab.dll"
#define DLL_NAME_OUTPUT_STREAM "output_stream.dll"
#define DLL_NAME_MALLOC "MALLOC.dll"
#define DLL_NAME_FILEIO "fileio.dll"
//==============================================================================
/* API_SCILAB */
#define ISNAMEDVAREXIST "isNamedVarExist"
#define GETNAMEDSCALARDOUBLE "getNamedScalarDouble"
#define CREATESNAMEDSINGLESTRING "createNamedSingleString"
//==============================================================================
/* OUTPUT_STREAM */
#define CLEARLASTERROR "clearLastError"
#define GETLASTERRORMESSAGE "getLastErrorMessage"
#define GETLASTERRORVALUE "getLastErrorValue"
//==============================================================================
/* CALL_SCILAB */
#define CALL_SCILABOPEN "Call_ScilabOpen"
#define TERMINATESCILAB "TerminateScilab"
#define SENDSCILABJOB "SendScilabJob"
#define SENDSCILABJOBS "SendScilabJobs"
#define GETLASTJOB "GetLastJob"
//==============================================================================
/* CORE */
#define GETTMPDIR "getTMPDIR"
#define SCIRUN "scirun_"
//==============================================================================
/* MALLOC */
#define MYHEAPFREE "MyHeapFree"
//==============================================================================
/* FILEIO */
#define DELETEAFILE "deleteafile"
//==============================================================================
#define MESSAGE_STACK_SIZE 5
//==============================================================================
typedef struct api_Err
{
    int iErr; /**< The error ID */
    int iMsgCount; /**< Error level */
    char* pstMsg[MESSAGE_STACK_SIZE]; /**< The error message */
} SciErr;
//==============================================================================
typedef struct api_Ctx
{
    char* pstName; /**< Function name */
} StrCtx, *pStrCtx;
//==============================================================================
/* API_SCILAB */
typedef int (*isNamedVarExistPROC)(void* _pvCtx, char* _pstName);
typedef int (*getNamedScalarDoublePROC)(void* _pvCtx, char* _pstName, double* _pdblReal);
typedef int (*createNamedSingleStringPROC)(void* _pvCtx, char* _pstName, const char* _pstStrings);
//==============================================================================
/* OUTPUT_STREAM */
typedef int (*clearLastErrorPROC)(void);
typedef const char** (*getLastErrorMessagePROC)(int *iNbLines);
typedef int (*getLastErrorValuePROC)(void);
//==============================================================================
/* CALL_SCILAB */
typedef int (*Call_ScilabOpenPROC)(char* SCIpath, BOOL advancedMode, char *ScilabStartup, int Stacksize);
typedef BOOL (*TerminateScilabPROC)(char *ScilabQuit);
typedef int (*SendScilabJobPROC)(char *job); 
typedef int (*SendScilabJobsPROC)(char **jobs, int numberjobs);
typedef BOOL (*GetLastJobPROC)(char *JOB,int nbcharsJOB);
//==============================================================================
/* CORE */
typedef char* (*getTMPDIRPROC)(void);
typedef int (*scirunPROC)(char *startupCode, long int startupCode_len);
//==============================================================================
/* MALLOC */
typedef void (*MyHeapFreePROC)(LPVOID lpAddress, char *fichier, int ligne);
//==============================================================================
/* FILEIO */
typedef BOOL (*deleteafilePROC)(char *filename);
//==============================================================================
/* API_SCILAB */
static isNamedVarExistPROC dynlib_isNamedVarExist = NULL;
static getNamedScalarDoublePROC dynlib_getNamedScalarDouble = NULL;
static createNamedSingleStringPROC dynlib_createNamedSingleString = NULL;
//==============================================================================
/* OUTPUT_STREAM */
static clearLastErrorPROC dynlib_clearLastError = NULL;
static getLastErrorMessagePROC dynlib_getLastErrorMessage = NULL;
static getLastErrorValuePROC dynlib_getLastErrorValue = NULL;
//==============================================================================
/* CALL_SCILAB */
static Call_ScilabOpenPROC dynlib_Call_ScilabOpen = NULL;
static TerminateScilabPROC dynlib_TerminateScilab = NULL;
static SendScilabJobPROC dynlib_SendScilabJob = NULL;
static SendScilabJobsPROC dynlib_SendScilabJobs = NULL;
static GetLastJobPROC dynlib_GetLastJob = NULL;
//==============================================================================
/* CORE */
//==============================================================================
static getTMPDIRPROC dynlib_getTMPDIR = NULL;
static scirunPROC dynlib_scirun = NULL;
//==============================================================================
/* MALLOC */
static MyHeapFreePROC dynlib_MyHeapFree = NULL;
//==============================================================================
/* FILEIO */
static deleteafilePROC dynlib_deleteafile = NULL;
//==============================================================================
static HINSTANCE hLibScilabDll = NULL;
static HINSTANCE hCallScilabDll = NULL;
static HINSTANCE hApi_ScilabDll = NULL;
static HINSTANCE hOutput_StreamDll = NULL;
static HINSTANCE hMALLOCDll = NULL;
static HINSTANCE hFILEIODll = NULL;
//==============================================================================
static StrCtx* pvApiCtx = NULL; // Only one instance in Scilab 5.x (API_SCILAB)
//==============================================================================
static BOOL bSymbolsLoaded = FALSE;
//==============================================================================
static void initApiCtx(void)
{
    if (pvApiCtx == NULL)
    {
        pvApiCtx = (StrCtx*)malloc(sizeof(StrCtx));
        if (pvApiCtx)
        {
            pvApiCtx->pstName = _strdup("API_SCILAB");
        }
    }
}
//==============================================================================
int isNamedVarExist(const char* _pstName)
{
    return dynlib_isNamedVarExist(pvApiCtx, (char*) _pstName);
}
//==============================================================================
int getNamedScalarDouble(const char* _pstName, double* _pdblReal)
{
    return dynlib_getNamedScalarDouble(pvApiCtx, (char*) _pstName, _pdblReal);
}
//==============================================================================
int createNamedSingleString(void* _pvCtx, char* _pstName, const char* _pstStrings)
{
    return dynlib_createNamedSingleString(pvApiCtx, (char*) _pstName, _pstStrings);
}
//==============================================================================
/* CALL_SCILAB functions */
int callScilabOpen(const char* SCIpath, BOOL advancedMode, const char *ScilabStartup, int Stacksize)
{
    return dynlib_Call_ScilabOpen((char*)SCIpath, advancedMode, (char*)ScilabStartup, Stacksize);
}
//==============================================================================
BOOL terminateScilab(const char *ScilabQuit)
{
    return dynlib_TerminateScilab((char*)ScilabQuit);
}
//==============================================================================
int sendScilabJob(const char *job)
{
    return dynlib_SendScilabJob((char*)job);
}
//==============================================================================
char *getLastScilabJob(void)
{
    char lastjob[4096]; // bsiz in scilab 4096 max
    if (dynlib_GetLastJob(lastjob, 4096))
    {
        return _strdup(lastjob);
    }
    return NULL;
}
//==============================================================================
/* OUTPUT_STREAM functions */
int clearLastError(void)
{
    return dynlib_clearLastError();
}
//==============================================================================
int getLastErrorValue(void)
{
    return dynlib_getLastErrorValue();
}
//==============================================================================
char* getLastErrorMessage(void)
{
    int iNbLines = 0, i = 0, nbChar = 0;
    const char **msgs = dynlib_getLastErrorMessage(&iNbLines);
    char *concat = NULL;

    for (i = 0; i < iNbLines; i++)
    {
        nbChar += (int)strlen(msgs[i]);
    }

    concat = (char*)malloc((nbChar + 1) * sizeof(char));
    if (concat)
    {
        strcpy(concat, "");
        for (i = 0; i < iNbLines; i++)
        {
            strcat(concat, msgs[i]);
        }
    }
    return concat;
}
//==============================================================================
/* CORE functions */
char* getTMPDIR(void)
{
    char *tmp = dynlib_getTMPDIR();
    char *TMPDIR = NULL;
    if (tmp)
    {
        TMPDIR = _strdup(tmp);
        dynlib_MyHeapFree(tmp, __FILE__, __LINE__);
        tmp = NULL;
    }
    return TMPDIR;
}
//==============================================================================
/* FILEIO functions */
BOOL deleteafile(char *filename)
{
    return dynlib_deleteafile(filename);
}
//==============================================================================
/* OTHERS */
BOOL loadSymbols(void)
{
    initApiCtx();

    if (bSymbolsLoaded == FALSE)
    {
        if (loadScilabDlls())
        {
            dynlib_isNamedVarExist = (isNamedVarExistPROC) GetProcAddress(hApi_ScilabDll, ISNAMEDVAREXIST);
            dynlib_getNamedScalarDouble = (getNamedScalarDoublePROC) GetProcAddress(hApi_ScilabDll, GETNAMEDSCALARDOUBLE);
            dynlib_createNamedSingleString = (createNamedSingleStringPROC) GetProcAddress(hApi_ScilabDll, CREATESNAMEDSINGLESTRING);

            /* OUTPUT_STREAM */
            dynlib_clearLastError = (clearLastErrorPROC) GetProcAddress(hOutput_StreamDll, CLEARLASTERROR);
            dynlib_getLastErrorMessage = (getLastErrorMessagePROC) GetProcAddress(hOutput_StreamDll, GETLASTERRORMESSAGE);
            dynlib_getLastErrorValue = (getLastErrorValuePROC) GetProcAddress(hOutput_StreamDll, GETLASTERRORVALUE);
            
            /* CALL_SCILAB */
            dynlib_Call_ScilabOpen = (Call_ScilabOpenPROC) GetProcAddress(hCallScilabDll, CALL_SCILABOPEN);
            dynlib_TerminateScilab = (TerminateScilabPROC) GetProcAddress(hCallScilabDll, TERMINATESCILAB);
            dynlib_SendScilabJob = (SendScilabJobPROC) GetProcAddress(hCallScilabDll, SENDSCILABJOB);
            dynlib_SendScilabJobs = (SendScilabJobsPROC) GetProcAddress(hCallScilabDll, SENDSCILABJOBS);
            dynlib_GetLastJob = (GetLastJobPROC) GetProcAddress(hCallScilabDll, GETLASTJOB);

            /* CORE */
            dynlib_getTMPDIR = (getTMPDIRPROC) GetProcAddress(hLibScilabDll, GETTMPDIR);
            dynlib_scirun = (scirunPROC) GetProcAddress(hLibScilabDll, SCIRUN);

            /* MALLOC */
            dynlib_MyHeapFree = (MyHeapFreePROC) GetProcAddress(hMALLOCDll, MYHEAPFREE);

            /* FILEIO */
            dynlib_deleteafile = (deleteafilePROC) GetProcAddress(hFILEIODll, DELETEAFILE);


            if (dynlib_isNamedVarExist &&
                dynlib_getNamedScalarDouble &&
                dynlib_createNamedSingleString &&
                dynlib_clearLastError &&
                dynlib_getLastErrorMessage &&
                dynlib_getLastErrorValue &&
                dynlib_Call_ScilabOpen &&
                dynlib_TerminateScilab &&
                dynlib_SendScilabJob &&
                dynlib_SendScilabJobs &&
                dynlib_GetLastJob &&
                dynlib_getTMPDIR &&
                dynlib_scirun &&
                dynlib_MyHeapFree &&
                dynlib_deleteafile)
            {
                bSymbolsLoaded = TRUE;
            }
            else
            {
                bSymbolsLoaded = freeSymbols(FALSE);
            }
        }
        else
        {
            bSymbolsLoaded = FALSE;
        }
    }


    return bSymbolsLoaded;
}
//==============================================================================
BOOL freeSymbols(BOOL bFreeDlls)
{
    if (bSymbolsLoaded)
    {
        if (bFreeDlls) freeScilabDlls();

        if (dynlib_isNamedVarExist) dynlib_isNamedVarExist = NULL;
        if (dynlib_getNamedScalarDouble) dynlib_getNamedScalarDouble = NULL;
        if (dynlib_createNamedSingleString) dynlib_createNamedSingleString = NULL; 

        if (dynlib_clearLastError) dynlib_clearLastError = NULL;
        if (dynlib_getLastErrorMessage) dynlib_getLastErrorMessage = NULL;
        if (dynlib_getLastErrorValue) dynlib_getLastErrorValue = NULL;

        if (dynlib_Call_ScilabOpen) dynlib_Call_ScilabOpen = NULL;
        if (dynlib_TerminateScilab) dynlib_TerminateScilab = NULL;
        if (dynlib_SendScilabJob) dynlib_SendScilabJob = NULL;
        if (dynlib_SendScilabJobs) dynlib_SendScilabJobs = NULL;
        if (dynlib_GetLastJob) dynlib_GetLastJob = NULL;

        if (dynlib_getTMPDIR) dynlib_getTMPDIR = NULL;
        if (dynlib_scirun) dynlib_scirun = NULL;

        if (dynlib_MyHeapFree) dynlib_MyHeapFree = NULL;

        if (dynlib_deleteafile) dynlib_deleteafile = NULL;

        bSymbolsLoaded = FALSE;
    }

    return bSymbolsLoaded;
}
//==============================================================================
BOOL isSymbolsLoaded(void)
{
    return bSymbolsLoaded;
}
//==============================================================================
BOOL loadScilabDlls(void)
{
    if (!bSymbolsLoaded)
    {
        std::string strScilabPath = getScilabPath();
        if (!FileExists(strScilabPath.c_str())) return FALSE;

        std::string strLibScilabDllFullFilename(strScilabPath + std::string("\\bin\\" + std::string(DLL_NAME_LIBSCILAB)));
        if (!FileExists(strLibScilabDllFullFilename.c_str())) return FALSE;

        std::string strCallScilabDllFullFilename(strScilabPath + std::string("\\bin\\" + std::string(DLL_NAME_CALLSCILAB)));
        if (!FileExists(strCallScilabDllFullFilename.c_str())) return FALSE;

        std::string strApi_ScilabDllFullFilename(strScilabPath + std::string("\\bin\\" + std::string(DLL_NAME_API_SCILAB)));
        if (!FileExists(strApi_ScilabDllFullFilename.c_str())) return FALSE;

        std::string strOutput_StreamDllFullFilename(strScilabPath + std::string("\\bin\\" + std::string(DLL_NAME_OUTPUT_STREAM)));
        if (!FileExists(strOutput_StreamDllFullFilename.c_str())) return FALSE;

        std::string strMALLOCDllFullFilename(strScilabPath + std::string("\\bin\\" + std::string(DLL_NAME_MALLOC)));
        if (!FileExists(strMALLOCDllFullFilename.c_str())) return FALSE;

        std::string strFILEIODllFullFilename(strScilabPath + std::string("\\bin\\" + std::string(DLL_NAME_FILEIO)));
        if (!FileExists(strFILEIODllFullFilename.c_str())) return FALSE;

        hLibScilabDll = LoadLibraryEx(strLibScilabDllFullFilename.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
        if (hLibScilabDll == NULL) return FALSE;

        hCallScilabDll = LoadLibraryEx(strCallScilabDllFullFilename.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
        if (hCallScilabDll == NULL) return FALSE;

        hApi_ScilabDll = LoadLibraryEx(strApi_ScilabDllFullFilename.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
        if (hApi_ScilabDll == NULL) return FALSE;

        hOutput_StreamDll = LoadLibraryEx(strOutput_StreamDllFullFilename.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
        if (hOutput_StreamDll == NULL) return FALSE;

        hMALLOCDll = LoadLibraryEx(strMALLOCDllFullFilename.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
        if (hMALLOCDll == NULL) return FALSE;

        hFILEIODll = LoadLibraryEx(strFILEIODllFullFilename.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
        if (hFILEIODll == NULL) return FALSE;

        return TRUE;
    }
    return FALSE;
}
//==============================================================================
BOOL freeScilabDlls(void)
{
    if (bSymbolsLoaded)
    {
        if (hLibScilabDll)
        {
            FreeLibrary(hLibScilabDll);
            hLibScilabDll = NULL;
        }

        if (hCallScilabDll)
        {
            FreeLibrary(hCallScilabDll);
            hCallScilabDll = NULL;
        }

        if (hApi_ScilabDll)
        {
            FreeLibrary(hApi_ScilabDll);
            hApi_ScilabDll = NULL;
        }

        if (hOutput_StreamDll)
        {
            FreeLibrary(hOutput_StreamDll);
            hOutput_StreamDll = NULL;
        }

        if (hMALLOCDll)
        {
            FreeLibrary(hMALLOCDll);
            hMALLOCDll = NULL;
        }

        if (hFILEIODll)
        {
            FreeLibrary(hFILEIODll);
            hFILEIODll = NULL;
        }

        return TRUE;
    }
    return FALSE;
}
//==============================================================================
int fileExec(const char * filename)
{
    int iErr = -1;
    if (filename)
    {
        std::string commandLine = std::string("Err_FileExec = exec(\"") + std::string(filename) + 
            std::string("\", \"errcatch\");exit(Err_FileExec);");

        iErr = dynlib_scirun((char*)commandLine.c_str(), commandLine.length());
    }
    return iErr;
}
//==============================================================================
