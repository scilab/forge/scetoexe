// Allan CORNET - 2010

function bOK = scetoexe_buildsln(slnFile, isRelease)

  bOK = %F;
  if getos() == "Windows" & isfile(slnFile) then
    if haveacompiler() & (findmsvccompiler() <> 'unknown') then
      if isRelease then
        releaseMode = "Release";
      else
        releaseMode = "Debug";
      end
      
      if win64() then
        archMode = "x64";
      else
        archMode = "Win32";
      end
      
      cmdline = "devenv """ + slnFile + """ /Rebuild """ + releaseMode + "|" + archMode + """";
      [output, bOK, exitcode] = dos(cmdline);
    end
  end
  
endfunction
