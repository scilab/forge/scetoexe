// Allan CORNET - 2010

function bOK = scetoexe_generate(filenameSCE, directoryDest)

  function bOK = update_FILE_EMBEDDED(file_embedded, filenameSCE)
    bOK = %F;
    if isfile(file_embedded) then
      [path, fname, extension] = fileparts(filenameSCE);
      filenamecleaned = fname + extension;
      txt = mgetl(file_embedded);
      txt = [txt;
    			   "";
             "const char FILENAME[] = """ + filenamecleaned + """;";
             ""];
      bOK = mputl(txt, file_embedded);
    end
  endfunction
  
  function bOK = copyTemplate(directoryDest)
    bOK = %F;
    if isdir(directoryDest) then
      FILESTOCOPY = ["scilabWrap.h";
										"scilabWrap.cpp";
										"helpers_utils.h";
										"helpers_utils.cpp";
										"main.c";
										"getScilabPath.hxx";
										"getScilabPath.cpp";
										"FileExists.h";
										"FileExists.c"];
      bCopy = %T;
			for i = 1: size(FILESTOCOPY, "*")
			  bCopy = bCopy & copyfile(scetoexe_getRootPath() + "/src/c/" + FILESTOCOPY(i), directoryDest + "/" + FILESTOCOPY(i));
			end
			bOK = bCopy;
    end
  endfunction
  
  function bOK = generateVSsolution(filenameSCE, directoryDest)
    bOK = %F;
    
    VS_NAME = fileparts(filenameSCE, "fname");
    sln = mgetl(scetoexe_getRootPath() + "/src/c/scetoexe.sln");
    vcproj = mgetl(scetoexe_getRootPath() + "/src/c/scetoexe.vcproj");
    sln = strsubst(sln, "scetoexe", VS_NAME);
    vcproj = strsubst(vcproj, "scetoexe", VS_NAME);
    bOK = mputl(sln, directoryDest + "/" + VS_NAME + ".sln") & mputl(vcproj, directoryDest + "/" + VS_NAME + ".vcproj");
  endfunction


  bOK = %F;
  if isfile(filenameSCE) & isdir(directoryDest) then
     fullfileDest = directoryDest + filesep() + "FILE_EMBEDDED.h";
     if (scetoexe_reswarp(filenameSCE, fullfileDest)) then
       if (update_FILE_EMBEDDED(fullfileDest, filenameSCE)) then
          bOK = copyTemplate(directoryDest) & generateVSsolution(filenameSCE, directoryDest);
       end
     end
  end
endfunction
