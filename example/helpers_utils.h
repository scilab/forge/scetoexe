//==============================================================================
// Allan C0RNET - 2010
//==============================================================================
#ifndef __SCETOEXE_UTILS_H__
#define __SCETOEXE_UTILS_H__

#if __cplusplus
extern "C" {
#endif

/*
*/
int scetoexe_Initialize(void);

/*
*/
int scetoexe_ExecFile(void);

/*
*/
int scetoexe_Finish(void);

#if __cplusplus
}
#endif

#endif /* __SCETOEXE_UTILS_H__ */
//==============================================================================
