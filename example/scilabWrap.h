//==============================================================================
/* Allan CORNET - 2010 */
//==============================================================================
#ifndef __SCILABWRAP_H__
#define __SCILABWRAP_H__

#include <Windows.h>

#if __cplusplus
extern "C" {
#endif

int fileExec(const char * filename);

/* API_SCILAB functions */
int isNamedVarExist(const char* _pstName);
int getNamedScalarDouble(const char* _pstName, double* _pdblReal);
int createNamedSingleString(char* _pstName, const char* _pstStrings);

/* CALL_SCILAB functions */
int callScilabOpen(const char* SCIpath, BOOL advancedMode, const char *ScilabStartup, int Stacksize);
BOOL terminateScilab(const char *ScilabQuit);
int sendScilabJob(const char *job); 
char *getLastScilabJob(void);

/* OUTPUT_STREAM functions */
int clearLastError(void);
int getLastErrorValue(void);
char* getLastErrorMessage(void);

/* CORE */
char* getTMPDIR(void);

/* FILEIO */
BOOL deleteafile(char *filename);

/* others */
BOOL isSymbolsLoaded(void);
BOOL loadScilabDlls(void);
BOOL loadSymbols(void);
BOOL freeScilabDlls(void);
BOOL freeSymbols(BOOL bFreeDlls);

#if __cplusplus
}
#endif

#endif /* __SCILABWRAP_H__ */
//==============================================================================
